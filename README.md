This is a proof of concept. The code could be refactored to use enums instead of strings, as well as a look up class instead of a switch in each adapter.

even though the code is not up to my standards I include it here because it is a very exciting though experiment that i was able to get to work

the though experiment was: Can i build native views at run time. The short answer, YES!

by adding a layer of abstraction between UIElement data source methods and actually populating the correct data, I inserted an adapter to look at a config file and grab the correct view.

what this allows you to do is deploy once and load the config file on app launch. because the config file is not built into the app, you can update it and change native views and view controllers in the wiled without needing to redeploy the app.

in addition, if you have multiple customers using a different configuration of the same app, any elements you build for a single customer can now be offered for all, instant productization of any customizations built into the app!