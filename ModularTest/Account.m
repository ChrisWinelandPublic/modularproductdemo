//
//  Account.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "Account.h"

@implementation Account

@synthesize name,number,balance;

- (id)init{
    if([super init]){
        name = @"";
        number = @"";
        balance = @"";
    }
    return self;
}

- (void)setAccountWithName:(NSString *)na
                 andNumber:(NSString *)nu
                andBalance:(NSString *)ba{
    name = na;
    number = nu;
    balance = ba;
}

@end
