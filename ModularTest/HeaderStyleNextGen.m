//
//  HeaderStyleNextGen.m
//  ModularTest
//
//  Created by Chris Wineland on 7/30/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "HeaderStyleNextGen.h"

@implementation HeaderStyleNextGen

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        UIImageView* cardless = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200, 38)];
        [cardless setCenter:[self center]];
        [cardless setImage:[UIImage imageNamed:@"CardlessCash.png"]];
        [self addSubview:cardless];
        
    }
    return self;
}

@end
