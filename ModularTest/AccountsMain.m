//
//  AccountsMain.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AccountsMain.h"
#import "UserContext.h"
#import "HexToUIColorSupport.h"


@implementation AccountsMain

- (id)init{
    if(self = [super init]){
        adapter = [[AccountsMainTableViewAdapter alloc]init];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [[self navigationItem]setTitle:@"Accounts"];
    CGRect bounds = [[UIScreen mainScreen] bounds];
    
    [[self view]setBackgroundColor:[[UserContext singleton] getColorOfType:kComplementry]];
    accountsMainTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, bounds.size.width, bounds.size.height) style:UITableViewStyleGrouped];
    [accountsMainTableView setBackgroundView:nil];
    [accountsMainTableView setBackgroundColor:[UIColor clearColor]];
    [accountsMainTableView setDataSource:self];
    [accountsMainTableView setDelegate:self];
    [accountsMainTableView setSeparatorColor:[UIColor colorWithHexString:@"#585858"]];
    [accountsMainTableView setTableHeaderView:[adapter headerVewForTable]];
    [accountsMainTableView setTableFooterView:[adapter footerViewForTable]];
    [accountsMainTableView setRowHeight:[adapter rowHeightForCell]];
    [[self view]addSubview:accountsMainTableView];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[[UserContext singleton]accounts]count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [adapter cellForRowAtIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
