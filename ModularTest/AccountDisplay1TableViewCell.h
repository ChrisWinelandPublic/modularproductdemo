//
//  AccountDisplay1TableViewCell.h
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Account.h"

@interface AccountDisplay1TableViewCell : UITableViewCell{
    Account* displayedAccount;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andAccount:(Account*)account;

@end
