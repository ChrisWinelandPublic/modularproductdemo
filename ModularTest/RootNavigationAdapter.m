//
//  RootNavigationAdapter.m
//  ModularTest
//
//  Created by Chris Wineland on 7/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "RootNavigationAdapter.h"
#import "IIWrapController.h"
#import "IIViewDeckController.h"
#import "LeftNavViewController.h"

@implementation RootNavigationAdapter

- (UIViewController*)generateRootViewWithViews:(NSArray*)views andDelegate:(id)delegate{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: @"AppRootNavigationType"];
    if([(NSString*)obj compare:@"TabBarController"] == NSOrderedSame){
        return [self generateTabBarRootViewWithViews:views];
    } else {
        return [self generateSideNaveRootViewWithViews:views andDelegate:delegate];
    }
}

- (UIViewController*)generateTabBarRootViewWithViews:(NSArray*)views{
    UITabBarController* globalTab = [[UITabBarController alloc]init];
    [globalTab setViewControllers:views];
    return globalTab;
}

- (UIViewController*)generateSideNaveRootViewWithViews:(NSArray*)views andDelegate:(id)delegate{
    LeftNavViewController* leftView = [[LeftNavViewController alloc] initWithViews:views];
    IIViewDeckController* deckController = [[IIViewDeckController alloc] initWithCenterViewController:[views objectAtIndex:0] leftViewController:leftView];
    deckController.leftSize	= 44;
    [deckController setEnabled:YES];
    return deckController;
}

@end
