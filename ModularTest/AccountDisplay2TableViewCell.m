//
//  AccountDisplay2TableViewCell.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AccountDisplay2TableViewCell.h"

@implementation AccountDisplay2TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andAccount:(Account*)account{
    self = [self initWithStyle:style reuseIdentifier:reuseIdentifier];
    displayedAccount = [[Account alloc]init];
    displayedAccount = account;
    [self setUpDisplayedLables];
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setUpDisplayedLables{
    UILabel* accountBalance = [[UILabel alloc]initWithFrame:CGRectMake(9, 5, 110, 25)];
    [accountBalance setBackgroundColor:[UIColor clearColor]];
    [accountBalance setText:[displayedAccount balance]];
    [self addSubview:accountBalance];
    
    UILabel* accountName = [[UILabel alloc]initWithFrame:CGRectMake(200, 5, 115, 25)];
    [accountName setTextAlignment:NSTextAlignmentRight];
    [accountName setBackgroundColor:[UIColor clearColor]];
    [accountName setText:[displayedAccount name]];
    [self addSubview:accountName];
    
    UILabel* accountNumber = [[UILabel alloc]initWithFrame:CGRectMake(200, 30, 115, 10)];
    [accountNumber setTextAlignment:NSTextAlignmentRight];
    [accountNumber setBackgroundColor:[UIColor clearColor]];
    [accountNumber setText:[displayedAccount number]];
    [accountNumber setFont:[UIFont systemFontOfSize:10]];
    [self addSubview:accountNumber];
}

@end
