//
//  HeaderStyle1.m
//  ModularTest
//
//  Created by Chris Wineland on 7/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "HeaderStyle1.h"

@implementation HeaderStyle1

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        UILabel* textLable = [[UILabel alloc]initWithFrame:CGRectMake(9, 9, self.frame.size.width-18, self.frame.size.height-18)];
        [textLable setText:@"Balances may be incorrect due to some minor issues"];
        [textLable setFont:[UIFont boldSystemFontOfSize:15]];
        [textLable setNumberOfLines:0];
        [textLable setLineBreakMode:NSLineBreakByWordWrapping];
        [textLable setBackgroundColor:[UIColor clearColor]];
        [self addSubview:textLable];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
