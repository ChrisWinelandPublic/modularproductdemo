//
//  AccountsMainTableViewAdapter.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AccountsMainTableViewAdapter.h"
#import "AccountDisplay1TableViewCell.h"
#import "AccountDisplay2TableViewCell.h"
#import "AccountsDisplayNextGenTableViewCell.h"
#import "HeaderStyle1.h"
#import "HeaderStyleNextGen.h"
#import "FooterStyle1.h"
#import "UserContext.h"

@implementation AccountsMainTableViewAdapter

- (UIView*)headerVewForTable{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: @"AccountMainHeader"];
    if([(NSString*)obj compare:@"HeaderStyle1"]== NSOrderedSame){
        return [[HeaderStyle1 alloc]initWithFrame:CGRectMake(0, 0, 320, 75)];
    } else if([(NSString*)obj compare:@"HeaderStyleNextGen"]== NSOrderedSame){
        return [[HeaderStyleNextGen alloc]initWithFrame:CGRectMake(0, 0, 320, 55)];
    }
    return nil;
}

- (UIView*)footerViewForTable{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: @"AccountMainFooter"];
    if([(NSString*)obj compare:@"FooterStyle1"]== NSOrderedSame){
        return [[FooterStyle1 alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    }
    return nil;
}

- (UITableViewCell*)cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    UITableViewCell* cell;
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: @"AccountMainCellType"];
    if([(NSString*)obj compare:@"AccountDisplay1TableViewCell"]== NSOrderedSame){
        static NSString* cellID = @"display1";
        if(cell == nil){
            cell = [[AccountDisplay1TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID andAccount:[[[UserContext singleton] accounts] objectAtIndex:indexPath.row]];
        }
    } else if([(NSString*)obj compare:@"AccountDisplay2TableViewCell"]== NSOrderedSame){
        static NSString* cellID = @"display2";
        if(cell == nil){
            cell = [[AccountDisplay2TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID andAccount:[[[UserContext singleton] accounts] objectAtIndex:indexPath.row]];
        }

    } else if([(NSString*)obj compare:@"AccountDisplayNextGenTableViewCell"]== NSOrderedSame){
        static NSString* cellID = @"displayNextGen";
        if(cell == nil){
            cell = [[AccountsDisplayNextGenTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID andAccount:[[[UserContext singleton] accounts] objectAtIndex:indexPath.row]];
        }
    }
    return cell;
}

- (NSInteger)rowHeightForCell{
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: @"AccountMainCellType"];
    if([(NSString*)obj compare:@"AccountDisplayNextGenTableViewCell"]== NSOrderedSame){
        return 64.0;
    } else {
        return 44.0;
    }
}

@end
