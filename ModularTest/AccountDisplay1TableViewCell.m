//
//  AccountDisplay1TableViewCell.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AccountDisplay1TableViewCell.h"

@implementation AccountDisplay1TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andAccount:(Account*)account{
    
    self = [self initWithStyle:style reuseIdentifier:reuseIdentifier];
    displayedAccount = [[Account alloc]init];
    displayedAccount = account;
    [self setUpDisplayedLables];
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)setUpDisplayedLables{
    UILabel* accountName = [[UILabel alloc]initWithFrame:CGRectMake(9, 5, 180, 25)];
    [accountName setBackgroundColor:[UIColor clearColor]];
    [accountName setText:[NSString stringWithFormat:@"%@-%@",[displayedAccount name], [displayedAccount number]]];
    [accountName setFont:[UIFont systemFontOfSize:14]];
    [self addSubview:accountName];
    
    UILabel* accountBalance = [[UILabel alloc]initWithFrame:CGRectMake(200, 5, 115, 30)];
    [accountBalance setBackgroundColor:[UIColor clearColor]];
    [accountBalance setTextAlignment:NSTextAlignmentRight];
    [accountBalance setText:[displayedAccount balance]];
    [self addSubview:accountBalance];
}

@end
