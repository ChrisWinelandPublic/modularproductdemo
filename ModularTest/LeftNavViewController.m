//
//  LeftNavViewController.m
//  ModularTest
//
//  Created by Chris Wineland on 7/29/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "LeftNavViewController.h"
#import "UserContext.h"

@interface LeftNavViewController ()

@end

@implementation LeftNavViewController

@synthesize sideTableView;

- (id)initWithViews:(NSArray*)views{
    if(self = [super init]){
        selectableViews = [[NSArray alloc]initWithArray:views];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    [[self view]setBackgroundColor:[[UserContext singleton] getColorOfType:kComplementry]];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    sideTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, [[UIApplication sharedApplication] statusBarFrame].size.height, screenWidth-44, screenHeight-20) style:UITableViewStyleGrouped];
    [sideTableView setDelegate:self];
    [sideTableView setDataSource:self];
    [sideTableView setBackgroundView:nil];
    [sideTableView setBackgroundColor:[UIColor clearColor]];
    [sideTableView setUserInteractionEnabled:NO];
    [[self view]addSubview:sideTableView];
}

- (void)viewDidAppear:(BOOL)animated{
    [sideTableView selectRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [selectableViews count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *MyIdentifier = @"MyIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	if (cell == nil) {
		cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
	}
    [[cell textLabel]setText:[[[selectableViews objectAtIndex:indexPath.row] tabBarItem] title]];
    [[cell imageView]setImage:[[[selectableViews objectAtIndex:indexPath.row] tabBarItem] selectedImage]];
    return cell;
}

@end
