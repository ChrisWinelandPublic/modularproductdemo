//
//  AccountsDisplayNextGenTableViewCell.m
//  ModularTest
//
//  Created by Chris Wineland on 7/30/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "AccountsDisplayNextGenTableViewCell.h"
#import "UserContext.h"

@implementation AccountsDisplayNextGenTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andAccount:(Account *)account{
    self = [self initWithStyle:style reuseIdentifier:reuseIdentifier];
    displayedAccount = [[Account alloc]init];
    displayedAccount = account;
    [self setUpDisplayedLables];
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return self;
}

- (void)setUpDisplayedLables{
    UILabel* accountInfoLable = [[UILabel alloc]initWithFrame:CGRectMake(9, 6, 200, 14)];
    [accountInfoLable setBackgroundColor:[UIColor clearColor]];
    [accountInfoLable setFont:[UIFont systemFontOfSize:12]];
    [accountInfoLable setTextColor:[UIColor blackColor]];
    NSString* subActNum = [displayedAccount.number substringFromIndex:7];
    [accountInfoLable setText:[NSString stringWithFormat:@"%@-%@",displayedAccount.name,subActNum]];
    [[self contentView]addSubview:accountInfoLable];
    
    UILabel* accountBalanceLable = [[UILabel alloc]initWithFrame:CGRectMake(9, 20, 250, 25)];
    [accountBalanceLable setBackgroundColor:[UIColor clearColor]];
    [accountBalanceLable setFont:[UIFont systemFontOfSize:26]];
    [accountBalanceLable setTextColor:[[UserContext singleton] getColorOfType:kPrimary]];
    [accountBalanceLable setText:displayedAccount.balance];
    [[self contentView]addSubview:accountBalanceLable];
    
    UILabel* balanceType = [[UILabel alloc]initWithFrame:CGRectMake(9, 46, 200, 12)];
    [balanceType setBackgroundColor:[UIColor clearColor]];
    [balanceType setFont:[UIFont systemFontOfSize:10]];
    [balanceType setTextColor:[UIColor lightGrayColor]];
    [balanceType setText:[self getBalanceType]];
    [[self contentView]addSubview:balanceType];
}

- (NSString*)getBalanceType{
    if([displayedAccount.name compare:@"Line Credit"]==NSOrderedSame){
        return @"Current Balance";
    } else {
        return @"Available Balance";
    }
}

@end
