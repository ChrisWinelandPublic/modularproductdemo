//
//  UserContext.m
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "UserContext.h"
#import "Account.h"
#import "HexToUIColorSupport.h"

@implementation UserContext

static NSString * primaryColor = @"AppPrimaryColor";
static NSString * secondaryColor = @"AppSecondaryColor";
static NSString * complementryColor = @"AppComplementryColor";

@synthesize accounts;

- (id)init{
    if(self = [super init]){
        [self setColors];
        [self buildAccounts];
    }
    return self;
}

+ (id)singleton{
    static UserContext *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[self alloc] init];
    });
    return singleton;
}

-(UIColor*)getColorOfType:(appColorType)type{
    if(colors == nil){
        return [UIColor clearColor];
    }
    UIColor* returnColor;
    switch (type) {
        case kPrimary:{
            returnColor = [colors objectForKey:primaryColor];
        }
            break;
        case kSecondary:{
            returnColor = [colors objectForKey:secondaryColor];
        }
            break;
        case kComplementry:{
            returnColor = [colors objectForKey:complementryColor];
        }
            break;
        default:
            break;
    }
    return returnColor;
}

- (void)setColors{
    if(colors == nil){
        colors = [[NSMutableDictionary alloc]initWithCapacity:0];
    }
    NSString *path = [[NSBundle mainBundle] pathForResource: @"ConfigParameters" ofType: @"plist"];
    NSMutableDictionary *dictplist =[[NSMutableDictionary alloc] initWithContentsOfFile:path];
    id obj = [dictplist objectForKey: primaryColor];
    if(obj == nil){
        [colors setObject:[UIColor blackColor] forKey:primaryColor];
    } else {
        [colors setObject:[UIColor colorWithHexString:obj] forKey:primaryColor];
    }
    
    obj = [dictplist objectForKey: secondaryColor];
    if(obj == nil){
        [colors setObject:[UIColor blackColor] forKey:secondaryColor];
    } else {
        [colors setObject:[UIColor colorWithHexString:obj] forKey:secondaryColor];
    }
    
    obj = [dictplist objectForKey: complementryColor];
    if(obj == nil){
        [colors setObject:[UIColor blackColor] forKey:complementryColor];
    } else {
        [colors setObject:[UIColor colorWithHexString:obj] forKey:complementryColor];
    }
}

- (void)buildAccounts{
    accounts = [[NSMutableArray alloc]initWithCapacity:0];
    Account* acc = [[Account alloc]init];
    [acc setAccountWithName:@"Savings" andNumber:@"*******8349" andBalance:@"$4,009.87"];
    [accounts addObject:acc];
    Account* acc1 = [[Account alloc]init];
    [acc1 setAccountWithName:@"Checking" andNumber:@"*******5298" andBalance:@"$1,639.39"];
    [accounts addObject:acc1];
    Account* acc2 = [[Account alloc]init];
    [acc2 setAccountWithName:@"Line Credit" andNumber:@"*******0019" andBalance:@"$6,778.03"];
    [accounts addObject:acc2];
}


@end
