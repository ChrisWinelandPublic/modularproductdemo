//
//  AccountsMainTableViewAdapter.h
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountsMainTableViewAdapter : NSObject{
    
}

- (UIView*)headerVewForTable;
- (UIView*)footerViewForTable;
- (UITableViewCell*)cellForRowAtIndexPath:(NSIndexPath*)indexPath;
- (NSInteger)rowHeightForCell;

@end
