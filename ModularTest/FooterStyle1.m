//
//  FooterStyle1.m
//  ModularTest
//
//  Created by Chris Wineland on 7/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import "FooterStyle1.h"

@implementation FooterStyle1

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        UILabel* textLable = [[UILabel alloc]initWithFrame:CGRectMake(9, 0, self.frame.size.width-18, self.frame.size.height-18)];
        [textLable setText:@"Footers may or may not be important"];
        [textLable setFont:[UIFont systemFontOfSize:10]];
        [textLable setNumberOfLines:0];
        [textLable setLineBreakMode:NSLineBreakByWordWrapping];
        [textLable setBackgroundColor:[UIColor clearColor]];
        [self addSubview:textLable];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
