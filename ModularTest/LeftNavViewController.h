//
//  LeftNavViewController.h
//  ModularTest
//
//  Created by Chris Wineland on 7/29/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftNavViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    UITableView* sideTableView;
    NSArray* selectableViews;
    CGFloat screenHeight;
    CGFloat screenWidth;
}

@property (nonatomic, retain)UITableView* sideTableView;

- (id)initWithViews:(NSArray*)views;

@end
