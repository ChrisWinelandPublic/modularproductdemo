//
//  RootNavigationAdapter.h
//  ModularTest
//
//  Created by Chris Wineland on 7/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RootNavigationAdapter : NSObject{
    
}

- (UIViewController*)generateRootViewWithViews:(NSArray*)views andDelegate:(id)delegate;

@end
