//
//  AccountsMain.h
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountsMainTableViewAdapter.h"

@interface AccountsMain : UIViewController<UITableViewDataSource, UITableViewDelegate>{
    UITableView* accountsMainTableView;
    AccountsMainTableViewAdapter* adapter;
}

@end
