//
//  HexToUIColorSupport.h
//  ModularTest
//
//  Created by Chris Wineland on 7/28/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor(MBCategory)

+ (UIColor *)colorWithHex:(UInt32)col;
+ (UIColor *)colorWithHexString:(NSString *)str;


@end
