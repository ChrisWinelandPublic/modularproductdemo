//
//  UserContext.h
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    kPrimary,
    kSecondary,
    kComplementry
}appColorType;

@interface UserContext : NSObject{
    NSMutableArray* accounts;
    NSMutableDictionary* colors;
}

@property (nonatomic, strong)NSMutableArray* accounts;

+(id)singleton;
-(UIColor*)getColorOfType:(appColorType)type;

@end
