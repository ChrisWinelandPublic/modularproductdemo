//
//  Account.h
//  ModularTest
//
//  Created by Chris Wineland on 7/24/14.
//  Copyright (c) 2014 chrisWineland. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Account : NSObject{
    NSString* name;
    NSString* number;
    NSString* balance;
}

- (void)setAccountWithName:(NSString*)na andNumber:(NSString*)nu andBalance:(NSString*)ba;

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* number;
@property (nonatomic, retain) NSString* balance;

@end
